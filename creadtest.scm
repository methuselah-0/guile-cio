#!/usr/bin/guile
!#
(use-modules (json))

;; Either this way:
(define readf-lib (dynamic-link "./libcio.so"))
(dynamic-call "init_cio" readf-lib)
;;(display (read-file "./hello.txt"))
(define bigjsonstring (read-file "./big.json"))
;;(define bigjsonstring (read-file "./hello.txt"))
;;(define jsonscm (json-string->scm bigjsonstring))
;;(define bigjsonstringfromscm (scm->json-string jsonscm #:pretty #t))
;;(write-file "./big-copy.json" bigjsonstringfromscm)
(write-file "./big-copy.json" bigjsonstring)
;;(define b (write-file "./hello-copy.json" bigjsonstringfromscm))

;; This way works too, but will give the compilation message ";;;
;; ERROR: no code for module (cio)".
;; (load-extension "./libcio.so" "scm_init_cio_module")
;; (use-modules (cio))
;; (display (read-file "./hello.txt"))
