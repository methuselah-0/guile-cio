Compile with for example:
  gcc -lguile-2.0 -lgc -pthread -I/usr/include/guile/2.0 -o libcio.so -shared -fPIC cread.c

Run "guile-config link" and "guile-config compile" to get the pthread and -I and -L arguments correct.

